stages:
  - build
  - validate

variables:
  IMAGE_NAME: ${CI_REGISTRY}/garethahealy/devsecops-validate
  IMAGE_NAME_TAG: ${IMAGE_NAME}:${CI_COMMIT_REF_NAME}

build-push-image:
  stage: build
  image:
    name: registry.access.redhat.com/ubi9/podman@sha256:5c3e3c3f904f587cec60bc4538e105117cad0fc0bed75f8558a5a26b7d380223
  before_script:
    - echo ${CI_REGISTRY_PASSWORD} | podman login --username ${CI_REGISTRY_USER} --password-stdin ${CI_REGISTRY}
  script:
    - podman build . --tag ${IMAGE_NAME_TAG}
    - podman push ${IMAGE_NAME_TAG}

get-digest:
  stage: build
  needs: ["build-push-image"]
  image:
    name: quay.io/skopeo/stable:v1.13
    entrypoint: [""]
  before_script:
    - echo ${CI_REGISTRY_PASSWORD} | skopeo login --username ${CI_REGISTRY_USER} --password-stdin ${CI_REGISTRY}
  script:
    - skopeo inspect --format "{{.Digest}}" docker://${IMAGE_NAME_TAG} > pre-digest.txt
  artifacts:
    paths:
      - pre-digest.txt

sign-image:
  stage: build
  needs: ["get-digest"]
  image:
    name: gcr.io/projectsigstore/cosign:v2.1.1
    entrypoint: [""]
  variables:
    COSIGN_YES: "true"
  id_tokens:
    SIGSTORE_ID_TOKEN:
      aud: sigstore
  before_script:
    - echo ${CI_REGISTRY_PASSWORD} | cosign login --username ${CI_REGISTRY_USER} --password-stdin ${CI_REGISTRY}
    - export SHA_DIGEST=$(cat pre-digest.txt)
  script:
    - cosign sign ${IMAGE_NAME}@${SHA_DIGEST}

generate-sbom:
  stage: build
  needs: ["get-digest"]
  image:
    name: docker.io/anchore/syft:v0.85.0-debug
    entrypoint: [""]
  before_script:
    - echo ${CI_REGISTRY_PASSWORD} | /syft login --username ${CI_REGISTRY_USER} --password-stdin ${CI_REGISTRY}
  script:
    - /syft packages ${IMAGE_NAME_TAG} -o cyclonedx > syft-sbom.json
  artifacts:
    paths:
      - syft-sbom.json
      - pre-digest.txt

attach-sbom:
  stage: build
  needs: ["generate-sbom"]
  image:
    name: gcr.io/projectsigstore/cosign:v2.1.1
    entrypoint: [""]
  variables:
    COSIGN_YES: "true"
  id_tokens:
    SIGSTORE_ID_TOKEN:
      aud: sigstore
  before_script:
    - echo ${CI_REGISTRY_PASSWORD} | cosign login --username ${CI_REGISTRY_USER} --password-stdin ${CI_REGISTRY}
    - export SHA_DIGEST=$(cat pre-digest.txt)
    - export SBOM_DIGEST=$(echo $SHA_DIGEST | tr ':' '-')
  script:
    - cosign attach sbom --sbom syft-sbom.json --type cyclonedx ${IMAGE_NAME}@${SHA_DIGEST}
    - cosign sign ${IMAGE_NAME}:${SBOM_DIGEST}.sbom

generate-vuln:
  stage: build
  needs: ["get-digest"]
  image:
    name: ghcr.io/aquasecurity/trivy:0.43.1
    entrypoint: [""]
  before_script:
    - export SHA_DIGEST=$(cat pre-digest.txt)
  script:
    - trivy image --format cosign-vuln --output vuln.json ${IMAGE_NAME}@${SHA_DIGEST}
  artifacts:
    paths:
      - vuln.json
      - pre-digest.txt

attach-vuln:
  stage: build
  needs: ["generate-vuln"]
  image:
    name: gcr.io/projectsigstore/cosign:v2.1.1
    entrypoint: [""]
  variables:
    COSIGN_YES: "true"
  id_tokens:
    SIGSTORE_ID_TOKEN:
      aud: sigstore
  before_script:
    - echo ${CI_REGISTRY_PASSWORD} | cosign login --username ${CI_REGISTRY_USER} --password-stdin ${CI_REGISTRY}
    - export SHA_DIGEST=$(cat pre-digest.txt)
  script:
    - cosign attest --type vuln --predicate vuln.json ${IMAGE_NAME}@${SHA_DIGEST}

### Validate

containerfile-exists:
  stage: validate
  script:
    - test -f Dockerfile

containerimage-exists:
  stage: validate
  needs: ["containerfile-exists"]
  image:
    name: quay.io/skopeo/stable:v1.13
    entrypoint: [""]
  before_script:
    - echo ${CI_REGISTRY_PASSWORD} | skopeo login --username ${CI_REGISTRY_USER} --password-stdin ${CI_REGISTRY}
  script:
    - skopeo inspect --format "{{.Digest}}" docker://${IMAGE_NAME_TAG} > digest.txt
  artifacts:
    paths:
      - digest.txt

sbom-exists:
  stage: validate
  needs: ["containerimage-exists"]
  image:
    name: gcr.io/projectsigstore/cosign:v2.1.1
    entrypoint: [""]
  before_script:
    - echo ${CI_REGISTRY_PASSWORD} | cosign login --username ${CI_REGISTRY_USER} --password-stdin ${CI_REGISTRY}
    - export SHA_DIGEST=$(cat digest.txt)
  script:
    - cosign download sbom ${IMAGE_NAME}@${SHA_DIGEST}

attestation-exists:
  stage: validate
  needs: ["containerimage-exists"]
  image:
    name: gcr.io/projectsigstore/cosign:v2.1.1
    entrypoint: [""]
  before_script:
    - echo ${CI_REGISTRY_PASSWORD} | cosign login --username ${CI_REGISTRY_USER} --password-stdin ${CI_REGISTRY}
    - export SHA_DIGEST=$(cat digest.txt)
  script:
    - cosign download attestation ${IMAGE_NAME}@${SHA_DIGEST}

signature-exists:
  stage: validate
  needs: ["containerimage-exists"]
  image:
    name: gcr.io/projectsigstore/cosign:v2.1.1
    entrypoint: [""]
  before_script:
    - echo ${CI_REGISTRY_PASSWORD} | cosign login --username ${CI_REGISTRY_USER} --password-stdin ${CI_REGISTRY}
    - export SHA_DIGEST=$(cat digest.txt)
  script:
    - cosign download signature ${IMAGE_NAME}@${SHA_DIGEST}